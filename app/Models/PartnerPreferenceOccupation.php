<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartnerPreferenceOccupation extends Model
{
    use HasFactory;

    protected $table = 'partner_preference_occupation';
    protected $primaryKey = "id";

    protected $fillable = [
        'id',
        'user_id',
        'occupation',
    ];
}
