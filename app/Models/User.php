<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role','name', 'last_name', 'email', 'email_verified_at', 'password', 'date_of_birth', 'genders', 'annual_income', 'occupation', 'family_type', 'manglik', 'pp_manglik', 'min_pp_expected_income', 'mix_pp_expected_income'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function PPOccupation()
    {
        return $this->hasMany(PartnerPreferenceOccupation::class,'user_id','id');
    }

    public function PPFamilyType()
    {
        return $this->hasMany(PartnerPreferenceFamilyType::class,'user_id','id');//->pluck('family_type');
    }

    public function getPartnerOccupationAttribute()
    {
        return $this->PPOccupation->pluck('occupation');
    }

    public function getPartnerFamilyTypeAttribute()
    {
        return $this->PPFamilyType->pluck('family_type');//->toArrray();
    }

    public function getAge(){
        return Carbon::parse($this->attributes['date_of_birth'])->age;
    }
}
