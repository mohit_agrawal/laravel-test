<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PartnerPreferenceFamilyType;
use App\Models\PartnerPreferenceOccupation;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/', 'max:100'],
            'last_name' => ['required', 'regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:150', 'unique:users'],
            'password' => ['required'],
            'date_of_birth' => ['required'],
            'genders' => ['required'],
            'annual_income' => ['required','regex:/^([0-9.])*$/',],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $data['pp_expected_income'] =explode(' ', str_replace(['₹','-'], '',$data['pp_expected_income']));
        $data['minPPExpectedIncome'] = (Int) $data['pp_expected_income'][0];
        $data['mixPPExpectedIncome'] = (Int) $data['pp_expected_income'][2];
        $convertDate = Carbon::createFromFormat("d/m/Y", $data['date_of_birth']);
        $data['date_of_birth'] = Carbon::parse($convertDate)->format("Y-m-d");

        try
        {
          return DB::transaction(function () use ($data) {
            $user_data =  User::create([
                    'name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                    'date_of_birth' => $data['date_of_birth'],
                    'genders' => $data['genders'],
                    'annual_income' => $data['annual_income'],
                    'occupation' => isset($data['occupation'])?$data['occupation']:null,
                    'family_type' => isset($data['family_type'])?$data['family_type']:null,
                    'manglik' => isset($data['manglik'])?$data['manglik']:null,
                    'pp_manglik' => isset($data['pp_manglik'])?$data['pp_manglik']:null,
                    'min_pp_expected_income' => $data['minPPExpectedIncome'],
                    'mix_pp_expected_income' => $data['mixPPExpectedIncome']
                ]);
                //save user  multiple Partner Preference Family Type
                foreach($data['pp_family_type'] as $value_family_type){
                    PartnerPreferenceFamilyType::create([
                        'user_id' => $user_data->id,
                        'family_type'=> $value_family_type

                    ]);
                }
                //save user multiple Partner Preference Occupation
                foreach ($data['pp_occupation'] as $value_occupation) {
                    PartnerPreferenceOccupation::create([
                        'user_id' => $user_data->id,
                        'occupation'=> $value_occupation
                    ]);
                }
                return $user_data;
            });

        } catch (\Exception $error) {
            dd($error);
            //throw $th;
        }
    }
}
