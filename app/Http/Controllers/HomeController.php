<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('admin');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $AuthUserData = Auth::User();
        if( $AuthUserData && $AuthUserData->role == 1 ){
            return redirect('/user');
        }
        $AuthUserData = Auth::User();

        $genders =  $AuthUserData->genders == 'male' ? 'female' : 'male';
        //search partner occupation
        $partner_occupation = [];
        if($AuthUserData->partner_occupation->toArray()){
            $partner_occupation = implode('\',\'', $AuthUserData->partner_occupation->toArray());
        }
        //search partner family types
        $partner_family_type = [];
        if($AuthUserData->partner_family_type->toArray()){
            $partner_family_type = implode('\',\'',$AuthUserData->partner_family_type->toArray());
        }
        $manglik = "manglik = ".$AuthUserData->pp_manglik;
        $minAnnualIncome = "annual_income >= ".$AuthUserData->min_pp_expected_income;
        $mixAnnualIncome = "annual_income <= ".$AuthUserData->mix_pp_expected_income;
        $occupation = "occupation in ('".$partner_occupation."')";
       // dd($partner_family_type);
        $familyType = "family_type in ('".$partner_family_type."')";

        $match_percentage = DB::raw("
        (CASE
        WHEN $manglik  AND $minAnnualIncome AND $mixAnnualIncome AND $occupation AND $familyType
        THEN  100
        WHEN ($manglik AND $minAnnualIncome AND $mixAnnualIncome  AND $occupation ) OR ($minAnnualIncome AND $mixAnnualIncome And $occupation AND $familyType) OR ($occupation AND $familyType AND $manglik) OR ($manglik AND $minAnnualIncome AND $mixAnnualIncome AND $familyType)
        THEN  75
        WHEN ($manglik AND $minAnnualIncome AND $mixAnnualIncome) OR ($familyType  AND $occupation ) OR ($manglik AND $familyType )  OR ($minAnnualIncome AND $mixAnnualIncome AND $occupation ) OR ($manglik AND $occupation) OR ($minAnnualIncome AND $mixAnnualIncome AND $familyType )
        THEN  50
        WHEN $manglik OR $minAnnualIncome OR $mixAnnualIncome OR $occupation OR $familyType
        THEN  25
        ELSE 0 END) AS match_percentage");


        // $match_percentage = DB::raw("
        // (CASE
        // WHEN 0
        // THEN  25
        // ELSE 0 END) AS match_percentage");

        $ppDataList = User::select(['*',$match_percentage])
        ->whereRole(2)
        ->where('genders',$genders)->orderBy('match_percentage','DESC')->get();

        return view('home',compact('ppDataList'));
    }
}
