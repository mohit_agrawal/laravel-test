<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
       // $this->middleware('admin');

    }

    public function index(Request $request)
    {
        $AuthUserData = Auth::User();

        if($AuthUserData && $AuthUserData->role == 2 ){
            return  redirect()->route('home');
        }
        $userDataList = User::select(['*'])
        ->selectRaw("TIMESTAMPDIFF(YEAR, DATE(date_of_birth), current_date) AS age")
        ->whereRole(2);

        //dd($request->all());
        if($request->date_of_birth){
            $userDataList = $userDataList->where(DB::raw("TIMESTAMPDIFF(YEAR, DATE(date_of_birth), current_date)"),$request->date_of_birth);
        }
        if($request->gender){
            $userDataList = $userDataList->where("genders",$request->gender);
        }
        if($request->pp_expected_income){
            $data['pp_expected_income'] = explode(' ', str_replace(['₹','-'], '',$request->pp_expected_income));
            $minPPExpectedIncome = (Int) $data['pp_expected_income'][0];
            $mixPPExpectedIncome = (Int) $data['pp_expected_income'][2];
            $userDataList = $userDataList->where("annual_income",">=",$minPPExpectedIncome)
                                        ->where("annual_income","<=",$mixPPExpectedIncome);
        }
        if($request->family_type){
            $userDataList = $userDataList->where("family_type",$request->family_type);
        }
        if($request->manglik){
            $userDataList = $userDataList->where("manglik",$request->manglik);
        }

        $userDataList = $userDataList->orderBy('id','DESC')->get();

        return view('user',compact('userDataList'));
    }
}
