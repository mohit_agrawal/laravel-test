<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $AuthData = Auth::User();
        if (isset($AuthData)) {
            if (Auth::user() &&  Auth::user()->role == 1) {
                //return redirect('/user');
                return  redirect()->route('user');
            }else if (Auth::user() &&  Auth::user()->role == 2) {
                return redirect('/home');
                return  redirect()->route('home');
            }
        }
        // dd('sd2');
        return $next($request);
    }
}
