<?php

namespace App\Http\Middleware;

use App\Models\GeneralSection;
use Closure;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $AuthData = Auth::User();
        if (isset($AuthData)) {
            if (Auth::user() &&  Auth::user()->role == 1) {
                return redirect('/user');
            }else if (Auth::user() &&  Auth::user()->role == 2) {
                return redirect('/home');
            }
        }
        dd('sd');
        return $next($request);

    }
}
