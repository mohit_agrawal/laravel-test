-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2022 at 05:37 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_test2`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2022_02_19_124019_create_partner_preference_occupation', 1),
(5, '2022_02_19_124050_create_partner_preference_family_type', 1);

-- --------------------------------------------------------

--
-- Table structure for table `partner_preference_family_type`
--

CREATE TABLE `partner_preference_family_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `family_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partner_preference_family_type`
--

INSERT INTO `partner_preference_family_type` (`id`, `user_id`, `family_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'Joint family', '2022-02-20 20:20:31', '2022-02-20 20:20:31'),
(2, 1, 'Nuclear family', '2022-02-20 20:20:31', '2022-02-20 20:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `partner_preference_occupation`
--

CREATE TABLE `partner_preference_occupation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partner_preference_occupation`
--

INSERT INTO `partner_preference_occupation` (`id`, `user_id`, `occupation`, `created_at`, `updated_at`) VALUES
(1, 1, 'Private job', '2022-02-20 20:20:31', '2022-02-20 20:20:31'),
(2, 1, 'Business', '2022-02-20 20:20:31', '2022-02-20 20:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT 2,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` timestamp NULL DEFAULT NULL,
  `genders` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_income` int(11) DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `family_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manglik` smallint(6) DEFAULT NULL,
  `pp_manglik` smallint(6) DEFAULT NULL,
  `min_pp_expected_income` int(11) NOT NULL DEFAULT 0,
  `mix_pp_expected_income` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `last_name`, `email`, `email_verified_at`, `password`, `date_of_birth`, `genders`, `annual_income`, `occupation`, `family_type`, `manglik`, `pp_manglik`, `min_pp_expected_income`, `mix_pp_expected_income`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, 'mohit', 'agrawal', 'mohit@gmail.com', NULL, '$2y$10$BzUeR6704js70dxRvdsfjeKWXYpFGhgv/EkSvU5TcBxXsiNnHTN32', '1997-02-01 18:30:00', 'male', 25000, '1', '1', 2, 2, 15000, 200000, NULL, '2022-02-20 20:20:31', '2022-02-20 20:20:31'),
(2, 2, 'Desmond', 'Kessler', 'xkemmer@example.org', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-22 18:30:00', 'female', 63473, 'Business', 'Joint family', 2, NULL, 0, 0, 'UnIW4hBEmH', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(3, 2, 'Randall', 'Pfannerstill', 'zena.lemke@example.org', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-22 18:30:00', 'male', 6926, 'Business', 'Nuclear family', 1, NULL, 0, 0, 'OaDkj0c7Rz', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(4, 2, 'Enrico', 'Luettgen', 'kohler.jacklyn@example.org', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-02 18:30:00', 'male', 66593, 'Private job', 'Joint family', 1, NULL, 0, 0, 'EB26wziarf', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(5, 2, 'Sienna', 'Roberts', 'ladarius84@example.com', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-19 18:30:00', 'female', 67312, 'Private job', 'Nuclear family', 2, NULL, 0, 0, 'wP3XTMv0z8', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(6, 2, 'Gia', 'Schiller', 'vchristiansen@example.net', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-16 18:30:00', 'female', 30947, 'Government Job', 'Joint family', 1, NULL, 0, 0, '8RriKVT4xI', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(7, 2, 'Guido', 'Hintz', 'gbruen@example.org', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-23 18:30:00', 'male', 138688, 'Government Job', 'Nuclear family', 2, NULL, 0, 0, '8Duvfd2qMo', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(8, 2, 'Sam', 'Hauck', 'bethany31@example.org', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-24 18:30:00', 'female', 153225, 'Business', 'Joint family', 1, NULL, 0, 0, 'WUqXRDOb7a', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(9, 2, 'Roberto', 'Predovic', 'russel.trycia@example.com', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-28 18:30:00', 'female', 167162, 'Government Job', 'Nuclear family', 2, NULL, 0, 0, '2ydCK9x7UE', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(10, 2, 'Una', 'VonRueden', 'nyah33@example.net', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-23 18:30:00', 'male', 154570, 'Government Job', 'Joint family', 2, NULL, 0, 0, '4Px6nShINr', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(11, 2, 'Mia', 'Kling', 'jbeahan@example.net', '2022-02-20 20:34:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-08 18:30:00', 'female', 996, 'Business', 'Joint family', 1, NULL, 0, 0, 'tVtmwk2Sgw', '2022-02-20 20:34:42', '2022-02-20 20:34:42'),
(12, 2, 'Torrey', 'Davis', 'roscoe.kuhn@example.org', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-07 18:30:00', 'female', 183851, 'Business', 'Nuclear family', 2, NULL, 0, 0, 'EAnAx7F4GD', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(13, 2, 'Elza', 'Haag', 'grimes.guadalupe@example.com', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-11 18:30:00', 'male', 94584, 'Government Job', 'Joint family', 1, NULL, 0, 0, '2tYI8qUXXJ', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(14, 2, 'Ernesto', 'Bogisich', 'colt87@example.org', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-11 18:30:00', 'female', 153519, 'Government Job', 'Joint family', 1, NULL, 0, 0, 'JOQrdSLpu6', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(15, 2, 'Juliet', 'Wisozk', 'demond.kris@example.com', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-11 18:30:00', 'male', 11974, 'Government Job', 'Nuclear family', 2, NULL, 0, 0, 'GWJ6qwSpSg', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(16, 2, 'Rocky', 'Williamson', 'bernhard.adeline@example.org', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-29 18:30:00', 'male', 98153, 'Private job', 'Joint family', 2, NULL, 0, 0, 'jGhEvdnz8H', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(17, 2, 'Ceasar', 'D\'Amore', 'fheathcote@example.com', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-25 18:30:00', 'female', 195446, 'Private job', 'Joint family', 2, NULL, 0, 0, 'RZvU8TgZBT', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(18, 2, 'Destini', 'Homenick', 'tremaine67@example.com', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-01-24 18:30:00', 'male', 56807, 'Government Job', 'Nuclear family', 1, NULL, 0, 0, 'kv2AVdJZl3', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(19, 2, 'Mollie', 'Stark', 'nathanael59@example.com', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-06 18:30:00', 'female', 121916, 'Private job', 'Nuclear family', 2, NULL, 0, 0, 'CL6WjWwEnc', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(20, 2, 'Katlyn', 'Herzog', 'ward.eula@example.net', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-08 18:30:00', 'female', 189956, 'Business', 'Joint family', 1, NULL, 0, 0, 'aofUkFYaDV', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(21, 2, 'Hans', 'Gleichner', 'lhalvorson@example.org', '2022-02-20 20:36:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2022-02-03 18:30:00', 'female', 169936, 'Business', 'Joint family', 1, NULL, 0, 0, 'T2jAcJkDTP', '2022-02-20 20:36:52', '2022-02-20 20:36:52'),
(22, 1, 'Admin', NULL, 'admin@gmail.com', NULL, '$2y$10$SUNWkFA7HJ5lQK.va2kGBOjOlT/KADPRTKR29z/ueJkwEEbvTNh0K', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2022-02-20 20:36:52', '2022-02-20 20:36:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_preference_family_type`
--
ALTER TABLE `partner_preference_family_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partner_preference_family_type_user_id_foreign` (`user_id`);

--
-- Indexes for table `partner_preference_occupation`
--
ALTER TABLE `partner_preference_occupation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partner_preference_occupation_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `partner_preference_family_type`
--
ALTER TABLE `partner_preference_family_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partner_preference_occupation`
--
ALTER TABLE `partner_preference_occupation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `partner_preference_family_type`
--
ALTER TABLE `partner_preference_family_type`
  ADD CONSTRAINT `partner_preference_family_type_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `partner_preference_occupation`
--
ALTER TABLE `partner_preference_occupation`
  ADD CONSTRAINT `partner_preference_occupation_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
