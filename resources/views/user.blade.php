@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Users</h2>
    <form method="GET" action="">
        <div class="card-body">
            <div class="row mb-3">
                <label for="date_of_birth" class="col-md-1 col-form-label">{{ __('Age') }}</label>
                <div class="col-md-2">
                    <input id="date_of_birth" type="text" class="form-control" name="date_of_birth" value="{{isset($_GET['date_of_birth']) ?$_GET['date_of_birth']:''}}">
                </div>

                <label for="gender" class="col-md-1 col-form-label">{{ __('Gender') }}</label>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="form-radio">
                            <div class="radio radio-inline">
                                <label>
                                    <input name="gender"  type="radio" id="agent_player_flag" value="male" {{isset($_GET['gender']) && $_GET['gender'] == 'male' ?'checked':''}}> Male
                                </label>
                                <label>
                                    <input name="gender"  type="radio" id="agent_player_flag" value="female" {{isset($_GET['gender']) && $_GET['gender'] == 'female' ?'checked':''}}> Female
                                </label>
                            </div>
                            <div class="radio radio-inline">

                            </div>
                        </div>
                    </div>
                </div>
                <label for="pp_expected_income" class="col-md-1 col-form-label">{{ __('Income') }}</label>

                <div class="col-md-3">
                    <input type="text" name="pp_expected_income" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;" value="{{isset($_GET['pp_expected_income']) ?$_GET['pp_expected_income']:''}}">
                    <div id="slider-range"></div>
                </div>
            </div>
            <div class="row mb-3">
                <label for="family_type" class="col-md-2 col-form-label">{{ __('Family Type') }}</label>

                <div class="col-md-3">
                    @php $familyTypeArray = ['Joint family', 'Nuclear family']; @endphp
                    <select class="custom-select" name="family_type" class="form-control @error('family_type') is-invalid @enderror">
                        <option value=''>Select</option>
                        @if(isset($familyTypeArray))
                            @foreach($familyTypeArray as $key=>$familyTypeVal)
                                <option value="{{ $key+1 }}" {{isset($_GET['family_type']) && $_GET['family_type'] != ''&& $_GET['family_type'] == $key ?'selected':''}}>{{$familyTypeVal}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <label for="manglik" class="col-md-1 col-form-label">{{ __('Manglik') }}</label>
                <div class="col-md-2">
                    @php $manglikArray = ['Yes', 'No']; @endphp
                    <select class="custom-select" name="manglik" class="form-control">
                        <option value=''>Select</option>
                        @if(isset($manglikArray))
                            @foreach($manglikArray as $key=>$manglikVal)
                                <option value="{{ $key+1 }}" {{isset($_GET['manglik']) && $_GET['manglik'] != '' && $_GET['manglik'] == $key ?'selected':''}}>{{$manglikVal}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Filter') }}
                    </button>
                </div>
                <div class="col-md-1">
                    <a class="btn btn-primary" href="/user">
                        {{ __('Reset') }}
                    </a>
                </div>
            </div>
        </div>
    </form>
    <table class="table">
      <thead>
        <tr>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Gender</th>
          <th>Annual Income</th>
          <th>Occupation</th>
          <th>Family Type</th>
          <th>Manglik</th>
          <th>age</th>
        </tr>
      </thead>
      <tbody>
        @foreach($userDataList as $userData)
            <tr>
                <td>{{$userData->name}}</td>
                <td>{{$userData->last_name}}</td>
                <td>{{$userData->genders}}</td>
                <td>{{$userData->annual_income}}</td>
                <td>{{$userData->occupation}}</td>
                <td>{{($userData->family_type == 0)?'Joint family':($userData->manglik == 1)?'Nuclear family':''}}</td>
                <td>
                    @if($userData->manglik != '' && $userData->manglik == 1)
                         {{'Yes'}}
                    @elseif($userData->manglik != '' && $userData->manglik == 2)
                        {{'No'}}
                   @endif
                </td>
                <td>{{$userData->age}}</td>
            </tr>
        @endforeach
     </tbody>
    </table>
  </div>
  @stop
  @section('page-script')
<script>
    $( function() {
        $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 200000,
        values: [ 15000, 50000 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
        }
        });
        $( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +
        " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );
    } );
</script>
@endsection
