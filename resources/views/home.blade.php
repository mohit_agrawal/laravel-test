@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Partner Preferences</h2>
    <table class="table">
      <thead>
        <tr>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Annual Income</th>
          <th>Occupation</th>
          <th>Family Type</th>
          <th>Manglik</th>
          <th>Match Percentage</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ppDataList as $ppData)
            <tr>
                <td>{{$ppData->name}}</td>
                <td>{{$ppData->last_name}}</td>
                <td>{{$ppData->annual_income}}</td>
                <td>{{$ppData->occupation}}</td>
                <td>{{$ppData->family_type}}</td>
                <td>{{($ppData->manglik == 1)?'Yes':'No'}}</td>
                <td>{{$ppData->match_percentage}} %</td>
            </tr>
        @endforeach
     </tbody>
    </table>
  </div>
@endsection
