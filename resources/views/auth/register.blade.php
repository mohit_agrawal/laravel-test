@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="card-header"> Basic Information Section </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <label for="first_name" class="col-md-2 col-form-label">{{ __('First Name') }}  <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" autocomplete="first_name" autofocus>

                                    @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            {{-- </div>
                            <div class="row mb-3"> --}}
                                <label for="last_name" class="col-md-2 col-form-label">{{ __('Last Name') }} <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror"     name="last_name" value="{{ old('last_name') }}" autocomplete="last_name" autofocus>

                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="email" class="col-md-2 col-form-label">{{ __('Email') }} <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            {{-- </div>
                            <div class="row mb-3"> --}}
                                <label for="password" class="col-md-2 col-form-label">{{ __('Password') }} <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="date_of_birth" class="col-md-2 col-form-label">{{ __('Date OF Birth') }} <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <input id="date_of_birth" type="text" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ old('date_of_birth') }}" autocomplete="date_of_birth" data-provide="datepicker">

                                    @error('date_of_birth')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            {{-- </div>
                            <div class="row mb-3"> --}}
                                <label for="genders" class="col-md-2 col-form-label">{{ __('Gender') }} <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="form-radio">
                                            <div class="radio radio-inline">
                                                <label>
                                                    <input name="genders"  type="radio" id="agent_player_flag" value="male"  class=" @error('genders') is-invalid @enderror"> Male
                                                </label>
                                                <label>
                                                    <input name="genders"  type="radio" id="agent_player_flag" value="female"  class=" @error('genders') is-invalid @enderror"> Female
                                                </label>
                                            </div>
                                            <div class="radio radio-inline">

                                            </div>
                                        </div>
                                    </div>
                                    @error('genders')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="annual_income" class="col-md-2 col-form-label">{{ __('Annual Income') }} <span style="color:red;">*</span></label>

                                <div class="col-md-4">
                                    <input id="annual_income" type="number" class="form-control @error('annual_income') is-invalid @enderror" name="annual_income" value="{{ old('annual_income') }}" autocomplete="annual_income">

                                    @error('annual_income')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            {{-- </div>
                            <div class="row mb-3"> --}}
                                <label for="occupation" class="col-md-2 col-form-label">{{ __('Occupation') }}</label>

                                <div class="col-md-4">
                                    @php $occupationArray = ['Private job', 'Government Job', 'Business']; @endphp
                                    <select class="custom-select" name="occupation" class="form-control @error('occupation') is-invalid @enderror">
                                        <option value=''>Select</option>
                                        @if(isset($occupationArray))
                                            @foreach($occupationArray as $key=>$occupationTypeVal)
                                                <option value="{{ $occupationTypeVal }}">{{$occupationTypeVal}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('occupation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="family_type" class="col-md-2 col-form-label">{{ __('Family Type') }}</label>

                                <div class="col-md-4">
                                    @php $familyTypeArray = ['Joint family', 'Nuclear family']; @endphp
                                    <select class="custom-select" name="family_type" class="form-control @error('family_type') is-invalid @enderror">
                                        <option value=''>Select</option>
                                        @if(isset($familyTypeArray))
                                            @foreach($familyTypeArray as $key=>$familyTypeVal)
                                                <option value="{{ $familyTypeVal }}">{{$familyTypeVal}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('family_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            {{-- </div>
                            <div class="row mb-3"> --}}
                                <label for="manglik" class="col-md-2 col-form-label">{{ __('Manglik') }}</label>

                                <div class="col-md-4">
                                    @php $manglikArray = ['Yes', 'No']; @endphp
                                    <select class="custom-select" name="manglik" class="form-control @error('manglik') is-invalid @enderror">
                                        <option value=''>Select</option>
                                        @if(isset($manglikArray))
                                            @foreach($manglikArray as $key=>$manglikVal)
                                                <option value="{{ $key+1 }}">{{$manglikVal}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('manglik')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-header"> Partner Preference</div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <label for="pp_expected_income" class="col-md-4 col-form-label">{{ __('expected income') }}</label>

                                <div class="col-md-6">
                                    <input type="text" name="pp_expected_income" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                    <div id="slider-range"></div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="pp_occupation" class="col-md-2 col-form-label">{{ __('Occupation') }}</label>

                                <div class="col-md-4">
                                    <select class="custom-select" name="pp_occupation[]" class="form-control @error('pp_occupation') is-invalid @enderror" multiple>
                                        {{-- <option value=''>Select</option> --}}
                                        @if(isset($occupationArray))
                                            @foreach($occupationArray as $key=>$occupationVal)
                                                <option value="{{ $occupationVal }}">{{$occupationVal}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('pp_occupation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="family_type" class="col-md-2 col-form-label">{{ __('Family Type') }}</label>

                                <div class="col-md-4">
                                    @php $familyTypeArray = ['Joint family', 'Nuclear family']; @endphp
                                    <select class="custom-select" name="pp_family_type[]" class="form-control @error('family_type') is-invalid @enderror" multiple>
                                        {{-- <option value=''>Select</option> --}}
                                        @if(isset($familyTypeArray))
                                            @foreach($familyTypeArray as $key=>$familyTypeVal)
                                                <option value="{{ $familyTypeVal }}">{{$familyTypeVal}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('family_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="manglik" class="col-md-2 col-form-label">{{ __('Manglik') }}</label>

                                <div class="col-md-4">
                                    @php $manglikArray = ['Yes', 'No']; @endphp
                                    <select class="custom-select" name="pp_manglik" class="form-control @error('manglik') is-invalid @enderror">
                                        <option value=''>Select</option>
                                        @if(isset($manglikArray))
                                            @foreach($manglikArray as $key=>$manglikVal)
                                                <option value="{{ $key+1 }}">{{$manglikVal}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('manglik')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-script')
<script>
    $( function() {
        $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 200000,
        values: [ 15000, 50000 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
        }
        });
        $( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +
        " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );
    } );
</script>
@endsection
