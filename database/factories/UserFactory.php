<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $number =  random_int(0, 200000);
        return [
            'name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'date_of_birth' => Carbon::parse($this->faker->dateTimeThisMonth)->format("Y-m-d"),
            'genders' => $this->faker->randomElement(['male', 'female']),
            'annual_income' => $number,
            'occupation' => $this->faker->randomElement(['Private job', 'Government Job', 'Business']),
            'family_type' => $this->faker->randomElement(['Joint family', 'Nuclear family']),
            'manglik' => $this->faker->randomElement([1,2]),
        ];
    }
}
