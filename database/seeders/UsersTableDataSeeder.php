<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_data =  User::create([
            'name' => 'Admin',
            'role' => 1,
            'email' => 'admin@gmail.com',
            'password' => Hash::make(123456),
        ]);

    }
}
