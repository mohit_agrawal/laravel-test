<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('role')->default('2');
            $table->string('name', 100);
            $table->string('last_name', 100)->nullable();
            $table->string('email', 100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->timestamp('date_of_birth')->nullable();
            $table->string('genders',50)->nullable();
            $table->integer('annual_income')->nullable();
            $table->string('occupation',100)->nullable();
            $table->string('family_type',100)->nullable();
            $table->smallInteger('manglik')->nullable();
            $table->smallInteger('pp_manglik')->nullable();
            $table->integer('min_pp_expected_income')->default('0');
            $table->integer('mix_pp_expected_income')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
